# cenapt/openldap

This is a container that serves an OpenLDAP (using a Centos base image).

Current features are:
- Samba integration
- SSL integration (with support for Let's encrypt type of certificates)

Tag latest maps to the master branch of bitbucket (and I wrote that just to get an auto-build):


Source here: https://bitbucket.org/cenapt/docker-openldap

## Usage

This example will work with a Let's encrypt config.

    docker volume create openldap_db
    docker volume create openldap_config
    docker run -it --rm -v openldap_db:/var/lib/ldap -v openldap_config:/etc/openldap/slapd.d \
     -e SLAPD_DC="dc=auth,dc=example,dc=net" -e SLAPD_MANAGER_PW="test" -e SLAPD_ROOT_PW="test" \
     -e SLAPD_NAME="Example.net Auth" -e SLAPD_SERVERNAME="auth" -e SAMBA_PW=test \
     -e TLS_CERT=cert.pem -e TLS_KEY=privkey.pem -e TLS_CACERT=chain.pem \
     -v /etc/letsencrypt/live/auth.example.net:/ssl --name auth cenapt/openldap

The server is then ready to listen on 389 and feel free to expose the port.
