#!/bin/bash

# Used to reduce memory usage
ulimit -n 1024

# Setup if it has not been already done
if [[ ! -f /var/lib/ldap/CONFIGURED ]]; then
    /setup.sh || (echo "Configuration failed" ; exit 1)
fi


/usr/sbin/slapd -u ldap -h "ldapi:/// ldap:/// ldaps:///" -d0
