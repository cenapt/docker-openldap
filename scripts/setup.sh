#!/bin/sh

# TODO:
# [ ] Remove all temp files
# [ ] Read config from configfile that gets deleted after first run
# [ ] Allow changing name of admin

export ADMIN_NAME="Manager"

function error_wrapper() {
    TMPFILE=$(mktemp)
    OUT=$(${1} < /dev/stdin 2> ${TMPFILE})
    if [[ $? -gt 0 ]]; then
        echo "Command failed with following output: "
        echo ${OUT}
        echo "Error: $(cat ${TMPFILE})"
        exit 1
    fi
    rm ${TMPFILE}
}

function ldap_add() {
    # Used with  << EOF
    # eg ldap_add << EOF
    # yourldif
    # EOF
    error_wrapper "ldapadd -Y EXTERNAL -H ldapi:///"
}

function ldap_modify() {
    error_wrapper "ldapmodify -Y EXTERNAL -H ldapi:///"
}


function ldap_add_as_manager() {
    error_wrapper "ldapadd -x -D cn=${ADMIN_NAME},${SLAPD_DC} -w ${SLAPD_MANAGER_PW}"
}


if [ ! -f /var/lib/ldap/DB_CONFIG ]; then
    cp /etc/openldap/local/DB_CONFIG /var/lib/ldap
fi

if [ ! -f /etc/openldap/slapd.d/cn=config.ldif ]; then
    export ROOTPW=$(slappasswd -s ${SLAPD_ROOT_PW})
    export MANAGERPW=$(slappasswd -s ${SLAPD_MANAGER_PW})
    export SAMBAPW=$(slappasswd -s ${SAMBA_PW})

    envsubst < /etc/openldap/local/slapd.ldif.tmpl > /etc/openldap/local/slapd.ldif
    envsubst < /etc/openldap/local/frontend.ldif.tmpl > /etc/openldap/local/frontend.ldif

    slapadd -n 0  -F /etc/openldap/slapd.d -l /etc/openldap/local/slapd.ldif
    slapadd  -F /etc/openldap/slapd.d -l /etc/openldap/local/frontend.ldif
    chown -R ldap:ldap /etc/openldap/slapd.d /var/lib/ldap
fi
# Starting only in local mode

#/usr/sbin/slapd -u ldap -h "ldapi:/// ldap://127.0.0.1/"  &
#sleep 5



# ldap_add <<EOF
# dn: olcDatabase={0}config,cn=config
# changetype: modify
# add: olcRootPW
# olcRootPW: ${ROOTPW}
# EOF

# ldap_add <<EOF
# dn: cn=module,cn=config
# objectClass: olcModuleList
# cn: module
# olcModulePath: /usr/lib64/openldap
# olcModuleLoad: back_mdb


# EOF


# # Setting TLS if requested

# if [ ! -z ${TLS_CERT} ] && [ ! -z ${TLS_KEY} ] && [ ! -z ${TLS_CACERT} ]; then

# ldap_add <<EOF
# dn: cn=config
# changetype: modify
# replace: olcTLSCertificateFile
# olcTLSCertificateFile: /ssl/${TLS_CERT}
# -
# replace: olcTLSCertificateKeyFile
# olcTLSCertificateKeyFile: /ssl/${TLS_KEY}
# -
# replace: olcTLSCACertificatePath
# olcTLSCACertificatePath: /usr/share/pki/ca-trust-source/
# -
# replace: olcTLSCACertificateFile
# olcTLSCACertificateFile: /ssl/${TLS_CACERT}
# EOF

# touch /var/lib/ldap/USING_TLS
# fi

# ldap_add < /etc/openldap/schema/cosine.ldif
# ldap_add < /etc/openldap/schema/nis.ldif
# ldap_add < /etc/openldap/schema/inetorgperson.ldif

# ldap_add <<EOF
# dn: olcDatabase={1}monitor,cn=config
# changetype: modify
# replace: olcAccess
# olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth"
#   read by dn.base="cn=${ADMIN_NAME},${SLAPD_DC}" read by * none
# EOF

# ldap_add <<EOF
# dn: olcDatabase={2}mdb,cn=config
# objectClass: olcDatabaseConfig
# objectClass: olcBdbConfig
# olcDatabase: {2}mdb
# olcDbDirectory: /var/lib/ldap
# olcSuffix: ${SLAPD_DC}
# olcRootDN: cn=${ADMIN_NAME},${SLAPD_DC}
# olcRootPW: ${MANAGERPW}
# EOF


# ldap_add <<EOF
# dn: olcDatabase={2}mdb,cn=config
# changetype: modify
# add: olcAccess
# olcAccess: {0}to attrs=userPassword,shadowLastChange by
#   dn="cn=${ADMIN_NAME},${SLAPD_DC}" write by anonymous auth by self write by * none
# olcAccess: {1}to dn.base="" by * read
# olcAccess: {2}to * by dn="cn=${ADMIN_NAME},${SLAPD_DC}" write by * read

# EOF

# ldap_add_as_manager <<EOF
# dn: ${SLAPD_DC}
# objectClass: top
# objectClass: dcObject
# objectclass: organization
# o: ${SLAPD_NAME}

# dn: cn=Manager,${SLAPD_DC}
# objectClass: organizationalRole
# cn: Manager
# description: Directory Manager

# dn: ou=People,${SLAPD_DC}
# objectClass: organizationalUnit
# ou: People

# dn: ou=Group,${SLAPD_DC}
# objectClass: organizationalUnit
# ou: Group
# EOF

# cat > /etc/openldap/schema_convert.conf <<EOF
# include /etc/openldap/schema/corba.schema
# include /etc/openldap/schema/core.schema
# include /etc/openldap/schema/cosine.schema
# include /etc/openldap/schema/duaconf.schema
# include /etc/openldap/schema/dyngroup.schema
# include /etc/openldap/schema/inetorgperson.schema
# include /etc/openldap/schema/java.schema
# include /etc/openldap/schema/misc.schema
# include /etc/openldap/schema/nis.schema
# include /etc/openldap/schema/openldap.schema
# include /etc/openldap/schema/ppolicy.schema
# include /etc/openldap/schema/collective.schema
# include /etc/openldap/schema/samba.schema
# EOF

# mkdir /etc/openldap/local/samba
# error_wrapper "slaptest -f /etc/openldap/schema_convert.conf -F /etc/openldap/local/samba"
# sed -i s/^dn.*/dn:\ cn=samba,\ cn=schema,\ cn=config/ /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^cn.*/cn:\ samba/ /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^structuralObjectClass:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^entryUUID:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^creatorsName:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^createTimestamp:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^entryCSN:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^modifiersName:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# sed -i s/^modifyTimestamp:// /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif

# ldap_add < /etc/openldap/local/samba/cn=config/cn=schema/cn={12}samba.ldif
# rm -rf /etc/openldap/local/samba

# ldap_modify <<EOF
# dn: olcDatabase={2}mdb,cn=config
# changetype: modify
# add: olcDbIndex
# olcDbIndex: uniqueMember eq,pres
# olcDbIndex: sambaSID eq
# olcDbIndex: sambaPrimaryGroupSID eq
# olcDbIndex: sambaGroupType eq
# olcDbIndex: sambaSIDList eq
# olcDbIndex: sambaDomainName eq
# olcDbIndex: default sub
# EOF

# ldap_add_as_manager <<EOF
# dn: cn=samba,${SLAPD_DC}
# objectClass: simpleSecurityObject
# objectClass: organizationalRole
# cn: samba
# description: Samba LDAP administrator
# userPassword: ${SAMBAPW}
# EOF

# ldap_modify <<EOF
# dn: olcDatabase={2}mdb,cn=config
# add: olcAccess
# olcAccess: to attrs=userPassword,shadowLastChange
#   by dn="cn=samba,${SLAPD_DC}" write by anonymous auth by self write by * none
# olcAccess: to * by dn="cn=samba,${SLAPD_DC}" write by * read
# EOF


touch /var/lib/ldap/CONFIGURED

echo "Configuration finished"
